<?php

$serverName = 'mysql';
$username = 'root';
$password = file_get_contents('/etc/mysql/password');
$dbname = 'devops';

$link = new mysqli($serverName, $username, $password, $dbname);

if(mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}